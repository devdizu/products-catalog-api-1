class Response {
    constructor(message = null, data = null, status = 200) {
      this.message = message;
      this.data = data;
      this.status = status;
    }
    get result() {
      return this;
    }
}
module.exports = Response;
  